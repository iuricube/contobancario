﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContoBancario.TipiMovimenti
{
    public interface IMovimento
    {
        public decimal Soldi { get; init; }
        public DateTime DataMovimento { get; init; }
        public string ToString();
    }
}
