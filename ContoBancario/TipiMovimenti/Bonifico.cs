﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContoBancario.TipiMovimenti
{
    public class Bonifico : IMovimento
    {
        public decimal Soldi { get; init; }
        public DateTime DataMovimento { get; init; }
        public string[] Causale { get; init; }
        public string IBAN { get; set; }

        public Bonifico()
        {
            //
        }

        public override string ToString()
        {
            return 
                $"IBAN Bersaglio : {IBAN}\n" +
                $"Soldi : {Soldi} €\n" +
                $"Data Movimento : {DataMovimento}\n" +
                $"Causale : " +
                $"{CausaleToString()}";
        }

        private string CausaleToString()
        {
            string output = "";
            foreach (var str in Causale)
            {
                output += $"\n  {str}";
            }
            return output;
        }
    }
}
