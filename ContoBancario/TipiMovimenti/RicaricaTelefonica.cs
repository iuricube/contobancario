﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContoBancario.TipiMovimenti
{
    public class RicaricaTelefonica : IMovimento
    {
        public decimal Soldi { get; init; }
        public DateTime DataMovimento { get; init; }
        public string Operatore { get; set; }
        public string NumeroTelefono { get; set; }

        public RicaricaTelefonica()
        {
            //
        }

        public override string ToString()
        {
            return 
                $"Numero Tel : {NumeroTelefono}\n" +
                $"Operatore : {Operatore}\n" +
                $"Soldi : {Soldi}\n" +
                $"Data Movimento : {DataMovimento}";
        }
    }
}
