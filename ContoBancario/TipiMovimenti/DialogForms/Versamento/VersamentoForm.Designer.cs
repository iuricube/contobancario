﻿
namespace ContoBancario.TipiMovimenti.DialogForms
{
    partial class VersamentoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SoldiNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.SoldiLabel = new System.Windows.Forms.Label();
            this.AnnullaButton = new System.Windows.Forms.Button();
            this.OkButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.SoldiNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // SoldiNumericUpDown
            // 
            this.SoldiNumericUpDown.Location = new System.Drawing.Point(66, 16);
            this.SoldiNumericUpDown.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.SoldiNumericUpDown.Name = "SoldiNumericUpDown";
            this.SoldiNumericUpDown.Size = new System.Drawing.Size(120, 23);
            this.SoldiNumericUpDown.TabIndex = 7;
            // 
            // SoldiLabel
            // 
            this.SoldiLabel.AutoSize = true;
            this.SoldiLabel.Location = new System.Drawing.Point(22, 18);
            this.SoldiLabel.Name = "SoldiLabel";
            this.SoldiLabel.Size = new System.Drawing.Size(39, 15);
            this.SoldiLabel.TabIndex = 6;
            this.SoldiLabel.Text = "Soldi :";
            // 
            // AnnullaButton
            // 
            this.AnnullaButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.AnnullaButton.Location = new System.Drawing.Point(103, 83);
            this.AnnullaButton.Name = "AnnullaButton";
            this.AnnullaButton.Size = new System.Drawing.Size(75, 23);
            this.AnnullaButton.TabIndex = 5;
            this.AnnullaButton.Text = "Annulla";
            this.AnnullaButton.UseVisualStyleBackColor = true;
            // 
            // OkButton
            // 
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OkButton.Location = new System.Drawing.Point(22, 83);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 23);
            this.OkButton.TabIndex = 4;
            this.OkButton.Text = "Invia";
            this.OkButton.UseVisualStyleBackColor = true;
            // 
            // VersamentoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(216, 131);
            this.Controls.Add(this.SoldiNumericUpDown);
            this.Controls.Add(this.SoldiLabel);
            this.Controls.Add(this.AnnullaButton);
            this.Controls.Add(this.OkButton);
            this.Name = "VersamentoForm";
            this.Text = "VersamentoForm";
            ((System.ComponentModel.ISupportInitialize)(this.SoldiNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown SoldiNumericUpDown;
        private System.Windows.Forms.Label SoldiLabel;
        private System.Windows.Forms.Button AnnullaButton;
        private System.Windows.Forms.Button OkButton;
    }
}