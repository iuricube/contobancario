﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContoBancario.TipiMovimenti
{
    public class Versamento : IMovimento
    {
        public decimal Soldi { get; init; }
        public DateTime DataMovimento { get; init; }

        public Versamento()
        {
            //
        }

        public override string ToString()
        {
            return
                $"Soldi : {Soldi}\n" +
                $"Data Movimento : {DataMovimento}";
        }
    }
}
