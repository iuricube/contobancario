﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ContoBancario.TipiMovimenti.DialogForms
{
    public partial class InvestimentoForm : Form
    {
        public InvestimentoForm()
        {
            InitializeComponent();
        }

        public Investimento GetInvestimento()
        {
            Investimento output = new()
            {
                Soldi = SoldiNumericUpDown.Value,
                SecondiInvestimento = (int) SecondiNumericUpDown.Value,
                DataMovimento = DateTime.Now
            };
            return output;
        }
    }
}
