﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContoBancario.TipiMovimenti
{
    public class Prelievo : IMovimento
    {
        public decimal Soldi { get; init; }
        public DateTime DataMovimento { get; init; }

        public Prelievo()
        {
            //
        }

        public string ToString()
        {
            return
                $"Soldi : {Soldi}\n" +
                $"Data Movimento : {DataMovimento}";
        }
    }
}
