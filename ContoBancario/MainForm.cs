﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ContoBancario.TipiMovimenti.DialogForms;

namespace ContoBancario
{
    public partial class MainForm : Form
    {
        public ContoCorrente Conto { get; private set; }
        public MainForm()
        {
            InitializeComponent();
            Conto = new ContoCorrente();

            IbanTextBox.Text = Conto.IBAN;

            Update_Timer.Tick += UpdateForm;
            Update_Timer.Interval = 500;
            Update_Timer.Start();
        }

        public void UpdateForm(object sender, EventArgs e)
        {
            SaldoTextBox.Text = Conto.Saldo.ToString() + " €";
            StoricoRichTextBox.Text = Conto.StoricoToString();
        }

        private void Prelievo_Button_Click(object sender, EventArgs e)
        {
            PrelievoForm dialog = new();
            if (dialog.ShowDialog(this) == DialogResult.OK)
            {
                Conto.FaiPrelievo(dialog.GetPrelievo());
            }
            dialog.Dispose();
            UpdateForm(sender, e);
        }

        private void Bonifico_Button_Click(object sender, EventArgs e)
        {
            BonificoForm dialog = new();
            if (dialog.ShowDialog(this) == DialogResult.OK)
            {
                Conto.FaiBonifico(dialog.GetBonifico());
            }
            dialog.Dispose();
            UpdateForm(sender, e);
        }

        private void Investimento_Button_Click(object sender, EventArgs e)
        {
            InvestimentoForm dialog = new();
            if (dialog.ShowDialog(this) == DialogResult.OK)
            {
                new Task(() => Conto.FaiInvestimento(dialog.GetInvestimento())).Start();
            }
            dialog.Dispose();
            UpdateForm(sender, e);
        }

        private void RicaricaTel_Button_Click(object sender, EventArgs e)
        {
            RicaricaTelForm dialog = new();
            if (dialog.ShowDialog(this) == DialogResult.OK)
            {
                Conto.FaiRicaricaTelefonica(dialog.GetRicaricaTelefonica());
            }
            dialog.Dispose();
            UpdateForm(sender, e);
        }

        private void VersamentoButton_Click(object sender, EventArgs e)
        {
            VersamentoForm dialog = new();
            if (dialog.ShowDialog(this) == DialogResult.OK)
            {
                Conto.FaiVersamento(dialog.GetVersamento());
            }
            dialog.Dispose();
            UpdateForm(sender, e);
        }
    }
}
