﻿
namespace ContoBancario.TipiMovimenti.DialogForms
{
    partial class BonificoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IbanLabel = new System.Windows.Forms.Label();
            this.SoldiLabel = new System.Windows.Forms.Label();
            this.IbanTextBox = new System.Windows.Forms.TextBox();
            this.SoldiNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.CausaleLabel = new System.Windows.Forms.Label();
            this.CausaleRichTextBox = new System.Windows.Forms.RichTextBox();
            this.AnnullaButton = new System.Windows.Forms.Button();
            this.OkButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.SoldiNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // IbanLabel
            // 
            this.IbanLabel.AutoSize = true;
            this.IbanLabel.Location = new System.Drawing.Point(12, 9);
            this.IbanLabel.Name = "IbanLabel";
            this.IbanLabel.Size = new System.Drawing.Size(40, 15);
            this.IbanLabel.TabIndex = 0;
            this.IbanLabel.Text = "IBAN :";
            // 
            // SoldiLabel
            // 
            this.SoldiLabel.AutoSize = true;
            this.SoldiLabel.Location = new System.Drawing.Point(12, 41);
            this.SoldiLabel.Name = "SoldiLabel";
            this.SoldiLabel.Size = new System.Drawing.Size(42, 15);
            this.SoldiLabel.TabIndex = 1;
            this.SoldiLabel.Text = "Soldi : ";
            // 
            // IbanTextBox
            // 
            this.IbanTextBox.Location = new System.Drawing.Point(58, 6);
            this.IbanTextBox.Name = "IbanTextBox";
            this.IbanTextBox.Size = new System.Drawing.Size(200, 23);
            this.IbanTextBox.TabIndex = 2;
            // 
            // SoldiNumericUpDown
            // 
            this.SoldiNumericUpDown.Location = new System.Drawing.Point(58, 39);
            this.SoldiNumericUpDown.Name = "SoldiNumericUpDown";
            this.SoldiNumericUpDown.Size = new System.Drawing.Size(81, 23);
            this.SoldiNumericUpDown.TabIndex = 3;
            // 
            // CausaleLabel
            // 
            this.CausaleLabel.AutoSize = true;
            this.CausaleLabel.Location = new System.Drawing.Point(12, 82);
            this.CausaleLabel.Name = "CausaleLabel";
            this.CausaleLabel.Size = new System.Drawing.Size(54, 15);
            this.CausaleLabel.TabIndex = 4;
            this.CausaleLabel.Text = "Causale :";
            // 
            // CausaleRichTextBox
            // 
            this.CausaleRichTextBox.Location = new System.Drawing.Point(12, 100);
            this.CausaleRichTextBox.Name = "CausaleRichTextBox";
            this.CausaleRichTextBox.Size = new System.Drawing.Size(250, 200);
            this.CausaleRichTextBox.TabIndex = 5;
            this.CausaleRichTextBox.Text = "";
            // 
            // AnnullaButton
            // 
            this.AnnullaButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.AnnullaButton.Location = new System.Drawing.Point(93, 316);
            this.AnnullaButton.Name = "AnnullaButton";
            this.AnnullaButton.Size = new System.Drawing.Size(75, 23);
            this.AnnullaButton.TabIndex = 7;
            this.AnnullaButton.Text = "Annulla";
            this.AnnullaButton.UseVisualStyleBackColor = true;
            // 
            // OkButton
            // 
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OkButton.Location = new System.Drawing.Point(12, 316);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 23);
            this.OkButton.TabIndex = 6;
            this.OkButton.Text = "Invia";
            this.OkButton.UseVisualStyleBackColor = true;
            // 
            // BonificoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(279, 351);
            this.Controls.Add(this.AnnullaButton);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.CausaleRichTextBox);
            this.Controls.Add(this.CausaleLabel);
            this.Controls.Add(this.SoldiNumericUpDown);
            this.Controls.Add(this.IbanTextBox);
            this.Controls.Add(this.SoldiLabel);
            this.Controls.Add(this.IbanLabel);
            this.Name = "BonificoForm";
            this.Text = "BonificoForm";
            ((System.ComponentModel.ISupportInitialize)(this.SoldiNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label IbanLabel;
        private System.Windows.Forms.Label SoldiLabel;
        private System.Windows.Forms.TextBox IbanTextBox;
        private System.Windows.Forms.NumericUpDown SoldiNumericUpDown;
        private System.Windows.Forms.Label CausaleLabel;
        private System.Windows.Forms.RichTextBox CausaleRichTextBox;
        private System.Windows.Forms.Button AnnullaButton;
        private System.Windows.Forms.Button OkButton;
    }
}