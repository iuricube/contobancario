﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ContoBancario.TipiMovimenti.DialogForms
{
    public partial class BonificoForm : Form
    {
        public BonificoForm()
        {
            InitializeComponent();
        }

        public Bonifico GetBonifico()
        {
            Bonifico output = new()
            {
                IBAN = IbanTextBox.Text,
                Soldi = SoldiNumericUpDown.Value,
                Causale = CausaleRichTextBox.Lines,
                DataMovimento = DateTime.Now
            };
            return output;
        }
    }
}
