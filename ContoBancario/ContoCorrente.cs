﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using CSUtils.Extensions;
using ContoBancario.TipiMovimenti;

namespace ContoBancario
{
    public class ContoCorrente
    {
        public decimal Saldo { get; set; }
        public string IBAN { get; set; }
        public List<IMovimento> Storico { get; set; }

        public ContoCorrente()
        {
            Saldo = 100;
            IBAN = Program.GetIban();
            Storico = new List<IMovimento>();
        }

        public void FaiBonifico(Bonifico bonifico)
        {
            if (bonifico.IBAN == IBAN)
            {
                Saldo += bonifico.Soldi;
            }
            else
            {
                Saldo -= bonifico.Soldi;
            }
            Storico.Add(bonifico);
        }

        public void FaiPrelievo(Prelievo prelievo)
        {
            Saldo -= prelievo.Soldi;
            Storico.Add(prelievo);
        }

        public void FaiInvestimento(Investimento investimento)
        {
            Saldo -= investimento.Soldi;
            Storico.Add(investimento);
            Thread.Sleep(investimento.SecondiInvestimento * 1000); // la funzione prende millisecondi, quindi moltiplico per mille per ottenere secondi
            Saldo += investimento.Soldi * (decimal) new Random().NextDouble(0.5, 2.0); // double e int non possono essere moltiplicati direttamente
        }

        public void FaiRicaricaTelefonica(RicaricaTelefonica ricarica)
        {
            Saldo -= ricarica.Soldi;
            Storico.Add(ricarica);
        }

        public void FaiVersamento(Versamento versamento)
        {
            Saldo += versamento.Soldi;
            Storico.Add(versamento);
        }

        public string StoricoToString()
        {
            string output = "";
            foreach (var item in Storico)
            {
                output = 
                    $"Tipo : {item.GetType().Name}\n" +
                    $"{item.ToString()}\n" +
                    $"=====\n" + output;
            }
            return output;
        }

        public override string ToString()
        {
            return
                $"IBAN : {IBAN}\n" +
                $"Saldo : {Saldo}\n" +
                $"Storico : \n{StoricoToString()}";
        }
    }
}
