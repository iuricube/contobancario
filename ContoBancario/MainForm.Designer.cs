﻿
namespace ContoBancario
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TabControl = new System.Windows.Forms.TabControl();
            this.Conto_TabPage = new System.Windows.Forms.TabPage();
            this.SaldoTextBox = new System.Windows.Forms.TextBox();
            this.IbanTextBox = new System.Windows.Forms.TextBox();
            this.VersamentoButton = new System.Windows.Forms.Button();
            this.RicaricaTel_Button = new System.Windows.Forms.Button();
            this.Investimento_Button = new System.Windows.Forms.Button();
            this.Bonifico_Button = new System.Windows.Forms.Button();
            this.Prelievo_Button = new System.Windows.Forms.Button();
            this.Saldo_Label = new System.Windows.Forms.Label();
            this.Iban_Label = new System.Windows.Forms.Label();
            this.Storico_TabPage = new System.Windows.Forms.TabPage();
            this.StoricoRichTextBox = new System.Windows.Forms.RichTextBox();
            this.Update_Timer = new System.Windows.Forms.Timer(this.components);
            this.TabControl.SuspendLayout();
            this.Conto_TabPage.SuspendLayout();
            this.Storico_TabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.Conto_TabPage);
            this.TabControl.Controls.Add(this.Storico_TabPage);
            this.TabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl.Location = new System.Drawing.Point(0, 0);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            this.TabControl.Size = new System.Drawing.Size(484, 361);
            this.TabControl.TabIndex = 1;
            // 
            // Conto_TabPage
            // 
            this.Conto_TabPage.Controls.Add(this.SaldoTextBox);
            this.Conto_TabPage.Controls.Add(this.IbanTextBox);
            this.Conto_TabPage.Controls.Add(this.VersamentoButton);
            this.Conto_TabPage.Controls.Add(this.RicaricaTel_Button);
            this.Conto_TabPage.Controls.Add(this.Investimento_Button);
            this.Conto_TabPage.Controls.Add(this.Bonifico_Button);
            this.Conto_TabPage.Controls.Add(this.Prelievo_Button);
            this.Conto_TabPage.Controls.Add(this.Saldo_Label);
            this.Conto_TabPage.Controls.Add(this.Iban_Label);
            this.Conto_TabPage.Location = new System.Drawing.Point(4, 24);
            this.Conto_TabPage.Name = "Conto_TabPage";
            this.Conto_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.Conto_TabPage.Size = new System.Drawing.Size(476, 333);
            this.Conto_TabPage.TabIndex = 0;
            this.Conto_TabPage.Text = "Conto";
            this.Conto_TabPage.UseVisualStyleBackColor = true;
            // 
            // SaldoTextBox
            // 
            this.SaldoTextBox.Location = new System.Drawing.Point(54, 53);
            this.SaldoTextBox.Name = "SaldoTextBox";
            this.SaldoTextBox.ReadOnly = true;
            this.SaldoTextBox.Size = new System.Drawing.Size(200, 23);
            this.SaldoTextBox.TabIndex = 10;
            // 
            // IbanTextBox
            // 
            this.IbanTextBox.Location = new System.Drawing.Point(54, 23);
            this.IbanTextBox.Name = "IbanTextBox";
            this.IbanTextBox.ReadOnly = true;
            this.IbanTextBox.Size = new System.Drawing.Size(200, 23);
            this.IbanTextBox.TabIndex = 9;
            // 
            // VersamentoButton
            // 
            this.VersamentoButton.Location = new System.Drawing.Point(9, 216);
            this.VersamentoButton.Name = "VersamentoButton";
            this.VersamentoButton.Size = new System.Drawing.Size(139, 23);
            this.VersamentoButton.TabIndex = 8;
            this.VersamentoButton.Text = "Fai Versamento";
            this.VersamentoButton.UseVisualStyleBackColor = true;
            this.VersamentoButton.Click += new System.EventHandler(this.VersamentoButton_Click);
            // 
            // RicaricaTel_Button
            // 
            this.RicaricaTel_Button.Location = new System.Drawing.Point(9, 187);
            this.RicaricaTel_Button.Name = "RicaricaTel_Button";
            this.RicaricaTel_Button.Size = new System.Drawing.Size(139, 23);
            this.RicaricaTel_Button.TabIndex = 7;
            this.RicaricaTel_Button.Text = "Fai Ricarica Telefonica";
            this.RicaricaTel_Button.UseVisualStyleBackColor = true;
            this.RicaricaTel_Button.Click += new System.EventHandler(this.RicaricaTel_Button_Click);
            // 
            // Investimento_Button
            // 
            this.Investimento_Button.Location = new System.Drawing.Point(9, 158);
            this.Investimento_Button.Name = "Investimento_Button";
            this.Investimento_Button.Size = new System.Drawing.Size(139, 23);
            this.Investimento_Button.TabIndex = 6;
            this.Investimento_Button.Text = "Fai Investimento";
            this.Investimento_Button.UseVisualStyleBackColor = true;
            this.Investimento_Button.Click += new System.EventHandler(this.Investimento_Button_Click);
            // 
            // Bonifico_Button
            // 
            this.Bonifico_Button.Location = new System.Drawing.Point(9, 129);
            this.Bonifico_Button.Name = "Bonifico_Button";
            this.Bonifico_Button.Size = new System.Drawing.Size(139, 23);
            this.Bonifico_Button.TabIndex = 5;
            this.Bonifico_Button.Text = "Fai Bonifico";
            this.Bonifico_Button.UseVisualStyleBackColor = true;
            this.Bonifico_Button.Click += new System.EventHandler(this.Bonifico_Button_Click);
            // 
            // Prelievo_Button
            // 
            this.Prelievo_Button.Location = new System.Drawing.Point(9, 100);
            this.Prelievo_Button.Name = "Prelievo_Button";
            this.Prelievo_Button.Size = new System.Drawing.Size(139, 23);
            this.Prelievo_Button.TabIndex = 4;
            this.Prelievo_Button.Text = "Fai Prelievo";
            this.Prelievo_Button.UseVisualStyleBackColor = true;
            this.Prelievo_Button.Click += new System.EventHandler(this.Prelievo_Button_Click);
            // 
            // Saldo_Label
            // 
            this.Saldo_Label.AutoSize = true;
            this.Saldo_Label.Location = new System.Drawing.Point(8, 56);
            this.Saldo_Label.Name = "Saldo_Label";
            this.Saldo_Label.Size = new System.Drawing.Size(42, 15);
            this.Saldo_Label.TabIndex = 2;
            this.Saldo_Label.Text = "Saldo :";
            // 
            // Iban_Label
            // 
            this.Iban_Label.AutoSize = true;
            this.Iban_Label.Location = new System.Drawing.Point(8, 26);
            this.Iban_Label.Name = "Iban_Label";
            this.Iban_Label.Size = new System.Drawing.Size(40, 15);
            this.Iban_Label.TabIndex = 0;
            this.Iban_Label.Text = "IBAN :";
            // 
            // Storico_TabPage
            // 
            this.Storico_TabPage.Controls.Add(this.StoricoRichTextBox);
            this.Storico_TabPage.Location = new System.Drawing.Point(4, 24);
            this.Storico_TabPage.Name = "Storico_TabPage";
            this.Storico_TabPage.Padding = new System.Windows.Forms.Padding(3);
            this.Storico_TabPage.Size = new System.Drawing.Size(476, 333);
            this.Storico_TabPage.TabIndex = 1;
            this.Storico_TabPage.Text = "Storico";
            this.Storico_TabPage.UseVisualStyleBackColor = true;
            // 
            // StoricoRichTextBox
            // 
            this.StoricoRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StoricoRichTextBox.Location = new System.Drawing.Point(3, 3);
            this.StoricoRichTextBox.Name = "StoricoRichTextBox";
            this.StoricoRichTextBox.ReadOnly = true;
            this.StoricoRichTextBox.Size = new System.Drawing.Size(470, 327);
            this.StoricoRichTextBox.TabIndex = 0;
            this.StoricoRichTextBox.Text = "";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 361);
            this.Controls.Add(this.TabControl);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.TabControl.ResumeLayout(false);
            this.Conto_TabPage.ResumeLayout(false);
            this.Conto_TabPage.PerformLayout();
            this.Storico_TabPage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl TabControl;
        private System.Windows.Forms.TabPage Conto_TabPage;
        private System.Windows.Forms.TabPage Storico_TabPage;
        private System.Windows.Forms.Label Iban_Label;
        private System.Windows.Forms.Label Saldo_Label;
        private System.Windows.Forms.Timer Update_Timer;
        private System.Windows.Forms.Button RicaricaTel_Button;
        private System.Windows.Forms.Button Investimento_Button;
        private System.Windows.Forms.Button Bonifico_Button;
        private System.Windows.Forms.Button Prelievo_Button;
        private System.Windows.Forms.RichTextBox StoricoRichTextBox;
        private System.Windows.Forms.Button VersamentoButton;
        private System.Windows.Forms.TextBox SaldoTextBox;
        private System.Windows.Forms.TextBox IbanTextBox;
    }
}

