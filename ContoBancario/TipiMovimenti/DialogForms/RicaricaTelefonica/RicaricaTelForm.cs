﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ContoBancario.TipiMovimenti.DialogForms
{
    public partial class RicaricaTelForm : Form
    {
        public RicaricaTelForm()
        {
            InitializeComponent();
        }

        public RicaricaTelefonica GetRicaricaTelefonica()
        {
            RicaricaTelefonica output = new()
            {
                NumeroTelefono = NumeroTelTextBox.Text,
                Operatore = OperatoreTextBox.Text,
                Soldi = SoldiNumericUpDown.Value,
                DataMovimento = DateTime.Now
            };
            return output;
        }
    }
}
