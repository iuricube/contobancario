/*
 * Usare .NET 5.0
 * Usare CSUtils.dll all'interno di lib
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using CSUtils.Extensions;

namespace ContoBancario
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        public static string GetIban()
        {
            return $"IT 12 X 12345 67890 {new Random().NextLong(100000000000, 999999999999)}";
        }
    }
}
