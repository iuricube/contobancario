﻿
namespace ContoBancario.TipiMovimenti.DialogForms
{
    partial class InvestimentoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SoldiNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.SoldiLabel = new System.Windows.Forms.Label();
            this.SecondiNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.SecondiLabel = new System.Windows.Forms.Label();
            this.AnnullaButton = new System.Windows.Forms.Button();
            this.OkButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.SoldiNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SecondiNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // SoldiNumericUpDown
            // 
            this.SoldiNumericUpDown.Location = new System.Drawing.Point(64, 23);
            this.SoldiNumericUpDown.Name = "SoldiNumericUpDown";
            this.SoldiNumericUpDown.Size = new System.Drawing.Size(81, 23);
            this.SoldiNumericUpDown.TabIndex = 5;
            // 
            // SoldiLabel
            // 
            this.SoldiLabel.AutoSize = true;
            this.SoldiLabel.Location = new System.Drawing.Point(18, 25);
            this.SoldiLabel.Name = "SoldiLabel";
            this.SoldiLabel.Size = new System.Drawing.Size(42, 15);
            this.SoldiLabel.TabIndex = 4;
            this.SoldiLabel.Text = "Soldi : ";
            // 
            // SecondiNumericUpDown
            // 
            this.SecondiNumericUpDown.Location = new System.Drawing.Point(154, 65);
            this.SecondiNumericUpDown.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.SecondiNumericUpDown.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.SecondiNumericUpDown.Name = "SecondiNumericUpDown";
            this.SecondiNumericUpDown.Size = new System.Drawing.Size(81, 23);
            this.SecondiNumericUpDown.TabIndex = 7;
            this.SecondiNumericUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // SecondiLabel
            // 
            this.SecondiLabel.AutoSize = true;
            this.SecondiLabel.Location = new System.Drawing.Point(18, 67);
            this.SecondiLabel.Name = "SecondiLabel";
            this.SecondiLabel.Size = new System.Drawing.Size(130, 15);
            this.SecondiLabel.TabIndex = 6;
            this.SecondiLabel.Text = "Secondi Investimento : ";
            // 
            // AnnullaButton
            // 
            this.AnnullaButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.AnnullaButton.Location = new System.Drawing.Point(99, 129);
            this.AnnullaButton.Name = "AnnullaButton";
            this.AnnullaButton.Size = new System.Drawing.Size(75, 23);
            this.AnnullaButton.TabIndex = 9;
            this.AnnullaButton.Text = "Annulla";
            this.AnnullaButton.UseVisualStyleBackColor = true;
            // 
            // OkButton
            // 
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OkButton.Location = new System.Drawing.Point(18, 129);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 23);
            this.OkButton.TabIndex = 8;
            this.OkButton.Text = "Invia";
            this.OkButton.UseVisualStyleBackColor = true;
            // 
            // InvestimentoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(250, 181);
            this.Controls.Add(this.AnnullaButton);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.SecondiNumericUpDown);
            this.Controls.Add(this.SecondiLabel);
            this.Controls.Add(this.SoldiNumericUpDown);
            this.Controls.Add(this.SoldiLabel);
            this.Name = "InvestimentoForm";
            this.Text = "InvestimentoForm";
            ((System.ComponentModel.ISupportInitialize)(this.SoldiNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SecondiNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown SoldiNumericUpDown;
        private System.Windows.Forms.Label SoldiLabel;
        private System.Windows.Forms.NumericUpDown SecondiNumericUpDown;
        private System.Windows.Forms.Label SecondiLabel;
        private System.Windows.Forms.Button AnnullaButton;
        private System.Windows.Forms.Button OkButton;
    }
}