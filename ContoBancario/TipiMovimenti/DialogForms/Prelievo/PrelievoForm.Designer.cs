﻿
namespace ContoBancario.TipiMovimenti.DialogForms
{
    partial class PrelievoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OkButton = new System.Windows.Forms.Button();
            this.AnnullaButton = new System.Windows.Forms.Button();
            this.SoldiLabel = new System.Windows.Forms.Label();
            this.SoldiNumericUpDown = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.SoldiNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // OkButton
            // 
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OkButton.Location = new System.Drawing.Point(12, 93);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 23);
            this.OkButton.TabIndex = 0;
            this.OkButton.Text = "Invia";
            this.OkButton.UseVisualStyleBackColor = true;
            // 
            // AnnullaButton
            // 
            this.AnnullaButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.AnnullaButton.Location = new System.Drawing.Point(93, 93);
            this.AnnullaButton.Name = "AnnullaButton";
            this.AnnullaButton.Size = new System.Drawing.Size(75, 23);
            this.AnnullaButton.TabIndex = 1;
            this.AnnullaButton.Text = "Annulla";
            this.AnnullaButton.UseVisualStyleBackColor = true;
            // 
            // SoldiLabel
            // 
            this.SoldiLabel.AutoSize = true;
            this.SoldiLabel.Location = new System.Drawing.Point(12, 28);
            this.SoldiLabel.Name = "SoldiLabel";
            this.SoldiLabel.Size = new System.Drawing.Size(39, 15);
            this.SoldiLabel.TabIndex = 2;
            this.SoldiLabel.Text = "Soldi :";
            // 
            // SoldiNumericUpDown
            // 
            this.SoldiNumericUpDown.Location = new System.Drawing.Point(56, 26);
            this.SoldiNumericUpDown.Name = "SoldiNumericUpDown";
            this.SoldiNumericUpDown.Size = new System.Drawing.Size(120, 23);
            this.SoldiNumericUpDown.TabIndex = 3;
            // 
            // PrelievoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(225, 132);
            this.Controls.Add(this.SoldiNumericUpDown);
            this.Controls.Add(this.SoldiLabel);
            this.Controls.Add(this.AnnullaButton);
            this.Controls.Add(this.OkButton);
            this.Name = "PrelievoForm";
            this.Text = "PrelievoForm";
            ((System.ComponentModel.ISupportInitialize)(this.SoldiNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.Button AnnullaButton;
        private System.Windows.Forms.Label SoldiLabel;
        private System.Windows.Forms.NumericUpDown SoldiNumericUpDown;
    }
}