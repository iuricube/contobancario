﻿
namespace ContoBancario.TipiMovimenti.DialogForms
{
    partial class RicaricaTelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NumeroTelLabel = new System.Windows.Forms.Label();
            this.NumeroTelTextBox = new System.Windows.Forms.TextBox();
            this.OperatoreTextBox = new System.Windows.Forms.TextBox();
            this.OperatoreLabel = new System.Windows.Forms.Label();
            this.SoldiLabel = new System.Windows.Forms.Label();
            this.SoldiNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.AnnullaButton = new System.Windows.Forms.Button();
            this.OkButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.SoldiNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // NumeroTelLabel
            // 
            this.NumeroTelLabel.AutoSize = true;
            this.NumeroTelLabel.Location = new System.Drawing.Point(12, 23);
            this.NumeroTelLabel.Name = "NumeroTelLabel";
            this.NumeroTelLabel.Size = new System.Drawing.Size(118, 15);
            this.NumeroTelLabel.TabIndex = 0;
            this.NumeroTelLabel.Text = "Numero di Telefono :";
            // 
            // NumeroTelTextBox
            // 
            this.NumeroTelTextBox.Location = new System.Drawing.Point(136, 20);
            this.NumeroTelTextBox.Name = "NumeroTelTextBox";
            this.NumeroTelTextBox.Size = new System.Drawing.Size(200, 23);
            this.NumeroTelTextBox.TabIndex = 1;
            // 
            // OperatoreTextBox
            // 
            this.OperatoreTextBox.Location = new System.Drawing.Point(136, 56);
            this.OperatoreTextBox.Name = "OperatoreTextBox";
            this.OperatoreTextBox.Size = new System.Drawing.Size(200, 23);
            this.OperatoreTextBox.TabIndex = 3;
            // 
            // OperatoreLabel
            // 
            this.OperatoreLabel.AutoSize = true;
            this.OperatoreLabel.Location = new System.Drawing.Point(12, 59);
            this.OperatoreLabel.Name = "OperatoreLabel";
            this.OperatoreLabel.Size = new System.Drawing.Size(123, 15);
            this.OperatoreLabel.TabIndex = 2;
            this.OperatoreLabel.Text = "Operatore Telefonico :";
            // 
            // SoldiLabel
            // 
            this.SoldiLabel.AutoSize = true;
            this.SoldiLabel.Location = new System.Drawing.Point(12, 92);
            this.SoldiLabel.Name = "SoldiLabel";
            this.SoldiLabel.Size = new System.Drawing.Size(39, 15);
            this.SoldiLabel.TabIndex = 4;
            this.SoldiLabel.Text = "Soldi :";
            // 
            // SoldiNumericUpDown
            // 
            this.SoldiNumericUpDown.Location = new System.Drawing.Point(57, 90);
            this.SoldiNumericUpDown.Name = "SoldiNumericUpDown";
            this.SoldiNumericUpDown.Size = new System.Drawing.Size(120, 23);
            this.SoldiNumericUpDown.TabIndex = 5;
            // 
            // AnnullaButton
            // 
            this.AnnullaButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.AnnullaButton.Location = new System.Drawing.Point(93, 143);
            this.AnnullaButton.Name = "AnnullaButton";
            this.AnnullaButton.Size = new System.Drawing.Size(75, 23);
            this.AnnullaButton.TabIndex = 7;
            this.AnnullaButton.Text = "Annulla";
            this.AnnullaButton.UseVisualStyleBackColor = true;
            // 
            // OkButton
            // 
            this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OkButton.Location = new System.Drawing.Point(12, 143);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 23);
            this.OkButton.TabIndex = 6;
            this.OkButton.Text = "Invia";
            this.OkButton.UseVisualStyleBackColor = true;
            // 
            // RicaricaTelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(351, 187);
            this.Controls.Add(this.AnnullaButton);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.SoldiNumericUpDown);
            this.Controls.Add(this.SoldiLabel);
            this.Controls.Add(this.OperatoreTextBox);
            this.Controls.Add(this.OperatoreLabel);
            this.Controls.Add(this.NumeroTelTextBox);
            this.Controls.Add(this.NumeroTelLabel);
            this.Name = "RicaricaTelForm";
            this.Text = "RicaricaTelForm";
            ((System.ComponentModel.ISupportInitialize)(this.SoldiNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label NumeroTelLabel;
        private System.Windows.Forms.TextBox NumeroTelTextBox;
        private System.Windows.Forms.TextBox OperatoreTextBox;
        private System.Windows.Forms.Label OperatoreLabel;
        private System.Windows.Forms.Label SoldiLabel;
        private System.Windows.Forms.NumericUpDown SoldiNumericUpDown;
        private System.Windows.Forms.Button AnnullaButton;
        private System.Windows.Forms.Button OkButton;
    }
}