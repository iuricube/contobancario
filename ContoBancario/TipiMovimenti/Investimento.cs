﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContoBancario.TipiMovimenti
{
    public class Investimento : IMovimento
    {
        public decimal Soldi { get; init; }
        public DateTime DataMovimento { get; init; }
        public int SecondiInvestimento { get; set; }

        public Investimento()
        {
            //
        }

        public override string ToString()
        {
            return 
                $"Secondi Investimento : {SecondiInvestimento}" +
                $"Soldi : {Soldi}\n" +
                $"Data Movimento : {DataMovimento}";
        }
    }
}
